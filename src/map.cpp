#include <iostream>
#include <string>
#include "gameobject.hpp"
#include "map.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Map::Map(){}

void Map::setRange(){

	ifstream map ("maze_map.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(map, aux);
		for(int u = 0; u < 50; u++){
			this->range[i][u] = aux[u];
		}
	}

	map.close();
}

void Map::getRange(){

		for(int i = 0; i < 20; i++){
			for(int u = 0; u < 50; u++){
		 		printw("%c", this->range[i][u]);
		 		if(u >= 49){
		 			printw("\n");
		 		}
		}
	}
}

void Map::addElements(int posx, int posy, char sprite){

	this->range[posy][posx] = sprite;

}

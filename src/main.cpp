#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "map.hpp"
#include "gameobject.hpp"
#include "player.hpp"

using namespace std;

int main(){

	Map * map = new Map();
	map->setRange();
	Player * player = new Player('@', 3, 3);

	//char a = 'a';
	char ch;

	while(TRUE){
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();

		map->addElements(player->getPos_x(), player->getPos_y(), player->getSprite());
		map->getRange();
		ch = getch();
		player->moviment();


		refresh();
		endwin();
	}

	return 0;
}

#include <iostream>
#include <string>
#include "gameobject.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Gameobject::Gameobject(){
  setPosx(0);
  setPosy(0);
  setSprite('#');
}
Gameobject::~Gameobject(){}

void Gameobject::setPosx(int posx){
  this->posx = posx;
}
int Gameobject::getPosx(){
  return posx;
}
void Gameobject::setPosy(int posy){
  this->posy = posy;
}
int Gameobject::getPosy(){
  return posy;
}
void Gameobject::setSprite(char sprite){
  this->sprite = sprite;
}
char Gameobject::getSprite(){
  return sprite;
}

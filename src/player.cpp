#include <iostream>
#include <string>
#include "player.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Player::Player(){}


Player::Player(char sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
}

void Player::setPos_x(int value){

	this->pos_x += value;

}

void Player::setPos_y(int value){
	this->pos_y += value;
}

void Player::setSprite(char sprite){
	this->sprite = sprite;
}

int Player::getPos_x(){
	return this->pos_x;
}

int Player::getPos_y(){
	return this->pos_y;
}

char Player::getSprite(){
	return this->sprite;
}

void Player::moviment(){

	char direction = 'l';

	direction = getch();

	if(direction == 'w'){
		this->setPos_y(-1);
	} else if (direction == 's'){
		this->setPos_y(1);
	} else if (direction == 'a'){
		this->setPos_x(-1);
	} else if (direction == 'd'){
		this->setPos_x(1);
	}

}

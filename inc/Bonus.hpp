#ifndef BONUS_HPP
#define BONUS_HPP

#include "GameObject.hpp"

class Bonus : public Gameobject {
private:
	int score;
public:
	Bonus();
	Bonus(int positionx, int positiony, char sprite);
	~Bonus();


	int getScore();
	void setScore(int score);
};
#endif

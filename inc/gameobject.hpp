#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

class Gameobject{
  // atributs
  int posx;
  int posy;
  char sprite;
public:
  Gameobject();
  ~Gameobject();

  int getPosx();
  void setPosx(int posx);
  int getPosy();
  void setPosy(int posy);
  char getSprite();
  void setSprite(char sprite);
};
#endif

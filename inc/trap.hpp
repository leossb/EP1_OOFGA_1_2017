#ifndef TRAP_HPP
#define TRAP_HPP
#include"gameobject.hpp"

class Trap : public Gameobject{
  int damage;

public:
  Trap();
  Trap(char sprite, int posx, int posy);
  ~Trap();

  void setDamage(int damage);
  int getDamage();

  void printTrap();
};
#endif

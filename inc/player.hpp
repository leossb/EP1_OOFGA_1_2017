#ifndef PLAYER_H
#define PLAYER_H

class Player{

	private:
		char sprite;
		int pos_x;
		int pos_y;

	public:
		Player();
		Player(char sprite, int posx, int posy);

		void setPos_x(int valor);
    int getPos_x();
    void setPos_y(int valor);
    int getPos_y();
    void setSprite(char sprite);
		char getSprite();

		void moviment();

};

#endif

#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <string>
#include "gameobject.hpp"

class Map {

	private:
		char range[20][50];

	public:
		Map();
		
		void setRange();
		void getRange();
		void addElements(int posx, int posy, char sprite);

};

#endif
